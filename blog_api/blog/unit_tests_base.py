from django.test import TestCase
from rest_framework.test import APIClient


class TestCaseBase(TestCase):
    """Base class for all unit test classes, to include generic behaviour."""

    api_client = APIClient()
