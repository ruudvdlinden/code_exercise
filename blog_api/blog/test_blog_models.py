import pytest

from blog.unit_tests_base import TestCaseBase
from blog.models import BlogUser, Post, Topic
from django.db import IntegrityError


class BlogModelsTest(TestCaseBase):
    """Unit tests to check model behaviour."""

    user = None
    topic = None

    def get_user(self):
        """Gets the test user, or create it if it doesn't exist yet."""

        user = self.user or BlogUser.objects.get_or_create(username='Trivial')[0]

        return user

    def get_topic(self):
        """Gets the test topic, or create it if it doesn't exist yet."""

        topic = self.topic or Topic.objects.get_or_create(name='Trivial')[0]

        return topic

    def test_post_requires_topic(self):
        """Tests required field 'topic' on a post instance."""

        with pytest.raises(IntegrityError) as exc:
            Post.objects.create(user=self.get_user())

        error_message = str(exc)

        assert 'NOT NULL constraint failed: blog_post.topic_id' in error_message

    def test_post_requires_user(self):
        """Tests required field 'user' on a post instance."""

        with pytest.raises(IntegrityError) as exc:
            Post.objects.create(topic=self.get_topic())

        error_message = str(exc)

        assert 'NOT NULL constraint failed: blog_post.user_id' in error_message
