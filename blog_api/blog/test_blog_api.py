import uuid
from urllib.parse import urlencode

from blog.unit_tests_base import TestCaseBase

from blog.models import BlogUser, Post, Topic
from django.urls import reverse
from rest_framework.status import HTTP_200_OK


class BlogApiTest(TestCaseBase):
    """Unit tests to check API behaviour."""

    @staticmethod
    def insert_topics(count):
        """Insert an amount of topics with a random name."""

        new_topics = [
            Topic.objects.create(name=str(uuid.uuid4())) for _counter in range(count)
        ]

        return new_topics

    def test_get_topics(self):
        """Tests the GET method to get all topics."""

        amount = 5

        new_topics = self.insert_topics(amount)

        response = self.api_client.get(reverse('topics'))

        assert response.status_code == HTTP_200_OK
        assert len(response.json()) == amount

        topics = sorted(response.json(), key=lambda topic: topic['id'])

        # Compare serialized properties with objects
        for index, topic in enumerate(topics):
            assert new_topics[index].pk == topic['id']
            assert new_topics[index].name == topic['name']

    def test_get_posts(self):
        """Tests the GET method to get all topics."""

        amount = 10
        post_title = 'Title of post!'

        user = BlogUser.objects.create(username='Basic user')
        topic = Topic.objects.create(name='Topic')

        # Create multiple posts
        for counter in range(amount):
            Post.objects.create(user=user, title=post_title, content='Content of post!', topic=topic)

        response = self.api_client.get(reverse('posts'))

        assert response.status_code == HTTP_200_OK
        assert len(response.json()) == amount

        result = response.json()[0]  # Taking the first of the results to test (nested) properties

        assert 'id' in result
        assert result['title'] == post_title
        assert result['topic'] == {
            'id': topic.pk,
            'name': topic.name
        }
        assert result['user'] == {
            'id': user.pk,
            'username': user.username
        }

    def test_get_posts_filtered_by_user(self):
        """Tests the GET method to get topics filtered by users."""

        user = BlogUser.objects.create(username='User')
        other_user = BlogUser.objects.create(username='other user')
        topic = Topic.objects.create(name='Topic')

        post = Post.objects.create(user=user, title='Title of post!', content='Content of post!', topic=topic)
        Post.objects.create(user=other_user, title='Title of post!', content='Content of post!', topic=topic)

        url = reverse('posts_for_user', kwargs={'user_id': user.pk})

        response = self.api_client.get(url)
        assert response.status_code == HTTP_200_OK

        result_post_ids = [post['id'] for post in response.json()]
        assert result_post_ids == [post.id]  # Only the post from the filtered user

    def test_get_posts_filtered_by_title(self):
        """Tests the GET method to get topics filtered by titles contains a supplied string."""

        user = BlogUser.objects.create(username='User')
        topic = Topic.objects.create(name='Topic')

        Post.objects.create(user=user, title='Title of post!', content='Content of post!', topic=topic)
        post = Post.objects.create(user=user, title='Title of post with unique word!', content='Content of post!',
                                   topic=topic)

        url = '{}?{}'.format(reverse('posts'), urlencode({'title__contains': 'unique'}))

        response = self.api_client.get(url)
        assert response.status_code == HTTP_200_OK

        result_post_ids = [post['id'] for post in response.json()]
        assert result_post_ids == [post.id]  # Only the post containing the specified string
