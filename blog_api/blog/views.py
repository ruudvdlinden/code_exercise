from rest_framework.response import Response
from rest_framework.views import APIView

from blog.models import Post, Topic
from blog.serializers import PostSerializer, TopicSerializer


class BaseAPIView(APIView):
    """Base class for all API views, to include generic behaviour."""

    model = None
    serializer = None
    accepted_filters = tuple()  # Default is no filters defined

    def get_query_set(self, options=None):
        """Filters the query set for all objects with the supplied filter arguments."""

        options = options or {}

        return self.model.objects.filter(**options)

    def get(self, request, **kwargs):
        """Gets all objects defined by the query set and returns a API response with serialized objects."""

        # Hard coding the accepted filter arguments as to control what can be filtered.
        options = {keyword: kwargs[keyword] for keyword in kwargs if keyword in self.accepted_filters}

        objects = list(self.get_query_set(options))

        serialized = self.serializer(objects, many=True)

        return Response(serialized.data)


class TopicList(BaseAPIView):
    model = Topic
    serializer = TopicSerializer


class PostList(BaseAPIView):
    model = Post
    serializer = PostSerializer

    accepted_filters = ('user_id', 'title__contains')

    def get_query_set(self, options=None, user_id=None):
        """Adds processing optional query parameters in to the generic filter functionality."""

        for query_param, query_value in self.request.query_params.items():
            if query_param in self.accepted_filters:
                options[query_param] = query_value

        return super(PostList, self).get_query_set(options)
