import random

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db import transaction

from blog.models import BlogUser, Post, Topic


class Command(BaseCommand):
    help = 'Inserts some BlogUsers, Topics and Posts. Deletes all existing objects so purely for testing purposes!'

    def add_arguments(self, parser):
        parser.add_argument('post_count', type=int)
        parser.add_argument('force', type=str)

    @transaction.atomic
    def handle(self, *args, **options):
        post_count = options['post_count'] or 2  # Minimal 2 for the user with multiple posts

        if options['force'] != 'force':
            raise ValueError('This manage command deletes ALL Posts, Topics and BlogUsers!'
                             'Specify "force" as second parameter to force running this command.')

        Post.objects.all().delete()
        Topic.objects.all().delete()
        BlogUser.objects.filter(is_superuser=False).delete()
        User.objects.filter(is_superuser=False).delete()

        user_without_posts = BlogUser.objects.create(username='User without posts')
        user_with_one_post = BlogUser.objects.create(username='User with exactly 1 post')
        user_with_multiple_posts = BlogUser.objects.create(username='User with multiple posts')

        topic_1 = Topic.objects.create(name='Topic 1')
        topic_2 = Topic.objects.create(name='Topic 2')

        Post.objects.create(user=user_with_one_post, title='Title!', content='Content!', topic=topic_1)

        # Create multiple posts and assign them to a random topic
        for counter in range(1, post_count + 1):
            Post.objects.create(user=user_with_multiple_posts,
                                title='Title of post {} !'.format(counter),
                                content='Content of post {}!'.format(counter),
                                topic=topic_1 if random.random() > .3 else topic_2)

        assert BlogUser.objects.count() == 3
        assert Topic.objects.count() == 2
        assert Post.objects.count() == post_count + 1

        assert not Post.objects.filter(user=user_without_posts).count()
        assert Post.objects.filter(user=user_with_one_post).count() == 1
        assert Post.objects.filter(user=user_with_multiple_posts).count() == post_count
