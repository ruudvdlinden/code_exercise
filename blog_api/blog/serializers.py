from rest_framework import serializers

from blog.models import BlogUser, Post, Topic


class TopicSerializer(serializers.ModelSerializer):
    """Defines model fields to be included in serialization."""

    class Meta:
        model = Topic
        fields = ('id', 'name')


class BlogUserSerializer(serializers.ModelSerializer):
    """Defines model fields to be included in serialization."""

    class Meta:
        model = BlogUser
        fields = ('id', 'username')


class PostSerializer(serializers.ModelSerializer):
    """Defines model fields to be included in serialization, and defines serializers for nested relations."""

    topic = TopicSerializer(many=False)
    user = BlogUserSerializer(many=False)

    class Meta:
        model = Post
        fields = ('id', 'title', 'content', 'topic', 'user')
