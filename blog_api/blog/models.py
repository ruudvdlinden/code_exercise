from django.contrib.auth.models import User
from django.db import models


class BlogUser(User):
    """Wrapper class for blog-specific code."""

    pass


class Topic(models.Model):
    name = models.CharField(max_length=128, blank=False)


class Post(models.Model):
    title = models.CharField(max_length=128)
    content = models.CharField(max_length=2 ** 16)
    user = models.ForeignKey(BlogUser, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
