from django.contrib import admin
from django.urls import path, re_path

from blog import views as blog_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('topics/', blog_views.TopicList.as_view(), name='topics'),
    re_path(r'^posts/$', blog_views.PostList.as_view(), name='posts'),
    re_path(r'^posts/user/(?P<user_id>\d+)/?$', blog_views.PostList.as_view(), name='posts_for_user'),
]
